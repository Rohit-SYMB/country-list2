package com.example.myglobe.CountryModel;

import com.google.gson.annotations.SerializedName;

public class countryDetails {
    public String name;
    public String capital;
    public String population;
    public String region;
    public String subregion;
    public String area;
    public String flag;

    public countryDetails(String name, String capital, String population, String region, String subregion, String area, String flag) {
        this.name = name;
        this.capital = capital;
        this.population = population;
        this.region = region;
        this.subregion = subregion;
        this.area = area;
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSubregion() {
        return subregion;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }

   public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}