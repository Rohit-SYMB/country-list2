package com.example.myglobe.network;

import com.example.myglobe.model.RetroPhoto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {
    @GET("/rest/v2/all")
    Call<List<RetroPhoto>> getAllPhotos();
}
