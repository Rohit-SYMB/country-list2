package com.example.myglobe.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myglobe.MainActivity;
import com.example.myglobe.R;
//import com.example.myglobe.Utils;
import com.example.myglobe.ShowDetails;
import com.example.myglobe.TimeDailog;
import com.example.myglobe.Utils;
import com.example.myglobe.model.RetroPhoto;

import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.O)
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolder> implements Filterable {

    List<RetroPhoto> dataList;
    List<RetroPhoto> dataListALL;
    public Context context;


    RetroPhoto retroPhoto;
    //public  String place=retroPhoto.getCountryName();

    public CustomAdapter(Context context, List<RetroPhoto> dataList) {
        this.context = context;
        this.dataList = dataList;
        this.dataListALL = new ArrayList<>(dataList);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<RetroPhoto> filteredList = new ArrayList<>();
            if (charSequence.toString().isEmpty()) {
                filteredList.addAll(dataListALL);
            } else {
                for (int i = 0; i < dataList.size(); i++) {
                    if ((dataList.get(i).getCountryName().toLowerCase().contains(charSequence.toString().toLowerCase())) ||
                            (dataList.get(i).getCapital().toLowerCase().contains(charSequence.toString().toLowerCase()))) {
                        filteredList.add(dataList.get(i));
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            dataList.clear();
            dataList.addAll((Collection<? extends RetroPhoto>) filterResults.values);
            notifyDataSetChanged();
        }
    };


    public class CustomViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        ImageButton button;
        ImageButton mapView;
        Button showDetails;
        TextView txtTitle;
        TextView txtcurren;
        TextView txtcap;
        TextView txtcode;
        TextView txtsym;
        TextView lang;
        TextView lang1;
        ImageView userAvatarView, flagView;

        CustomViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            txtcap = mView.findViewById(R.id.cap);
            txtTitle = mView.findViewById(R.id.title);
            flagView = mView.findViewById(R.id.coverImage);
            txtcurren = mView.findViewById(R.id.Curren);
            txtcode = mView.findViewById(R.id.Code);
            txtsym = mView.findViewById(R.id.symbol);
            lang = mView.findViewById(R.id.lang);
            lang1 = mView.findViewById(R.id.lang1);
            button = mView.findViewById(R.id.timebutton);
            mapView = mView.findViewById(R.id.mapButton);
            showDetails=mView.findViewById(R.id.show_details);
        }
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.custom_rows, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        RetroPhoto retroPhoto = dataList.get(position);
        final String place = retroPhoto.getCountryName();
        holder.txtTitle.setText(dataList.get(position).getCountryName());
        String capital = "(" + dataList.get(position).getCapital() + ")";
        holder.txtcap.setText(capital);
        String currency = retroPhoto.getCurrency().get(0).getName();
        String code = retroPhoto.getCurrency().get(0).getCode();
        String symbol = retroPhoto.getCurrency().get(0).getSymbol();
        String lang = retroPhoto.getLanguages().get(0).getLanguageName();
        holder.lang.setText(lang);
        // holder.lang1.setText(lang1);
        holder.txtcurren.setText(currency);
        holder.txtcode.setText(code);
        holder.txtsym.setText(symbol);
        String url = retroPhoto.getFlag();

        holder.showDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomAdapter.this.context, ShowDetails.class);
                intent.putExtra("place", place);
                context.startActivity(intent);
            }
        });

        String timezone = retroPhoto.getTimezones().get(0);

        ZoneId zoneId=ZoneId.of(timezone);

        LocalTime id1 = LocalTime.now(zoneId);


        Utils.fetchSvg(context, url, holder.flagView);
        holder.button.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                openDialog();
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            private void openDialog() {
                TimeDailog timeDailog = new TimeDailog();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    timeDailog.show(((AppCompatActivity) context).getSupportFragmentManager(), "timeDialog");
                }

            }
        });

        holder.mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "https://www.google.com/maps" + "/place" + "/" + place;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                context.startActivity(intent);
            }
        });
    }




    public void updateList(List<RetroPhoto> newList) {
        dataList = new ArrayList<>();
        dataList.addAll(newList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public void showDialog(String timeZone){

    }

}








//  ZonedDateTime zonedDateTime = ZonedDateTime.now();
  /*String timez = timezone.replace("UTC", "").replace(":", "");
        int newTimeZone = Integer.parseInt(timez);
        LocalTime localTime = LocalTime.now();

        Instant nowUtc=Instant.now();
        Integer offset = ZonedDateTime.now().getOffset();
        int utc = localTime+timez.to;
        */

   /* public void showDetails(String place){

        Intent intent = new Intent(this.context, ShowDetails.class);
        context.startActivity(intent);
    }*/

   /*  String result= id1.format(DateTimeFormatter.ofPattern("HH:MM:SS"));

        showDialog(result);

*/

        /*int uttc = Integer.parseInt(utc);
        //int newtime = uttc + 3600000 + newTimeZone;
          public int newwtime(newtime) {
            return newtime;
        }*/