package com.example.myglobe.model;

import com.google.gson.annotations.SerializedName;

public class Language {

    @SerializedName("name")
    private String languageName;

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }


}
