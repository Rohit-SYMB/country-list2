package com.example.myglobe.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.util.Currency;
import java.util.List;

public class RetroPhoto {
    @SerializedName("name")
    public String countryName;
    @SerializedName("capital")
    public String capital;
    @SerializedName("currencies")
     List<CurrencyData> currency;
    @SerializedName("languages")
     List<Language> languages;
    @SerializedName("flag")
    private String flag;
    @SerializedName("timezones")
    private List<String> timezones = null;
   /* @SerializedName("latlng")
    List<Lat> latlng;

    public List<Lat> getLatlng() {
        return latlng;
    }

    public void setLatlng(List<Lat> latlng) {
        this.latlng = latlng;
    }
*/

    public List<String> getTimezones() {
        return timezones;
    }

    public void setTimezones(List<String> timezones) {
        this.timezones = timezones;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public List<CurrencyData> getCurrency() {
        return currency;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public void setCurrency(List<CurrencyData> currency) {
        this.currency = currency;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }
}


