package com.example.myglobe.model;

import com.google.gson.annotations.SerializedName;

public class CurrencyData {
    @SerializedName("code")
    private String code;
    @SerializedName("name")
    private String name;

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    @SerializedName("symbol")
    private String symbol;
}
