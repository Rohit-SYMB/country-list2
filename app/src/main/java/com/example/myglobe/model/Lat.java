package com.example.myglobe.model;

import com.google.gson.annotations.SerializedName;

public class Lat {
    public int getLatlng() {
        return latlng;
    }

    public void setLatlng(int latlng) {
        this.latlng = latlng;
    }

    @SerializedName("latlng")
    private int latlng;
}
