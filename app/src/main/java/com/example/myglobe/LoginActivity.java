package com.example.myglobe;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myglobe.network.users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class LoginActivity extends AppCompatActivity {
    private EditText mobileNumberInput;
    private EditText nameInput;
    private static final String TAG = "LoginActivity";

    FirebaseDatabase database = FirebaseDatabase.getInstance();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button loginButton = findViewById(R.id.loginButton);
        mobileNumberInput =   findViewById(R.id.loginMobileNumber);
        nameInput= findViewById(R.id.loginName);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newMobileNumber = mobileNumberInput.getText().toString();
                String mobileNumber="+91"+newMobileNumber;

                if (mobileNumber.isEmpty() || mobileNumber.length() < 10) {
                    mobileNumberInput.setError("Enter mobile number");
                    mobileNumberInput.requestFocus();
                    return;
                }
                String phoneNumber = mobileNumber;
                addUsers();

                Intent intent = new Intent(LoginActivity.this, VerifyPhoneActivity.class);
                intent.putExtra("mobileNumber", mobileNumber);
                startActivity(intent);
            }
        });
       // Log.d("FCM TEST","TOKEN"+ FirebaseInstanceId.getInstance().getToken());


        NotificationChannel channel=new NotificationChannel(getString(R.string.MyNotification),"notification", NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager manager=getSystemService(NotificationManager.class);
        manager.createNotificationChannel(channel);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        //String token = task.getResult().getToken();
                        String token=FirebaseInstanceId.getInstance().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                       // Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

      //  myRef.setValue("Hello, World!");
    }
     DatabaseReference myRef = database.getReference("Users");

    private void addUsers() {
        String name=nameInput.getText().toString().trim();
        String phoneNumber=mobileNumberInput.getText().toString().trim();

        if (!TextUtils.isEmpty(name)&&!TextUtils.isEmpty(phoneNumber)){
            String id=myRef.push().getKey();
            users users=new users(name,phoneNumber);
            myRef.child(id).setValue(users);
        }
        else {
            Toast.makeText(this,"Enter mobile number",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(FirebaseAuth.getInstance().getCurrentUser()!=null){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


}
