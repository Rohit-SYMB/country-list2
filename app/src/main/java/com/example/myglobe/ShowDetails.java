package com.example.myglobe;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.widget.Toast;
import com.example.myglobe.CountryAdapter.CountryAdapter;
import com.example.myglobe.CountryModel.countryDetails;
import com.example.myglobe.CountryNetwork.ClientInstance;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ShowDetails extends AppCompatActivity {
    private RecyclerView recyclerView;
    private Object CountryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);

        recyclerView = findViewById(R.id.customRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ClientInstance.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        String place = getIntent().getStringExtra("place");
        ClientInstance api = retrofit.create(ClientInstance.class);
        Call<List<countryDetails>> call = api.getAllDetails(""+place);

        call.enqueue(new Callback<List<countryDetails>>() {
            @Override
            public void onResponse(Call<List<countryDetails>> call, Response<List<countryDetails>> response) {

                //List<countryDetails> countryDetails=response.body();
                showIt(response.body());
            }

            @Override
            public void onFailure(Call<List<countryDetails>> call, Throwable t) {

                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showIt(List<countryDetails> response) {
        CountryAdapter countryAdapter = new CountryAdapter(response, getApplicationContext());
        recyclerView.setAdapter(countryAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}