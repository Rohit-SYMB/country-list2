package com.example.myglobe.CountryAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myglobe.CountryModel.countryDetails;
import com.example.myglobe.R;
import com.example.myglobe.Utils;

import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {


    private List<countryDetails> infoList;
    private Context context;

    private static int currentPosition = 0;

    public CountryAdapter(List<countryDetails> infoList, Context context) {
        this.infoList = infoList;
        this.context = context;
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_custom_rows, parent, false);
        return new CountryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryAdapter.CountryViewHolder holder, int position) {
        countryDetails countryDetails = infoList.get(position);
        String name = "Country Name : " + infoList.get(position).getName();
        String capital = "Capital : " + infoList.get(position).getCapital();
        String population = "Population : " + infoList.get(position).getPopulation();
        String area = "Area : " + infoList.get(position).getArea();
        String region = "Region : " + infoList.get(position).getRegion();
        String subregion = "Subregion : " + infoList.get(position).getSubregion();
        holder.textViewName.setText(name);
        holder.textViewCapital.setText(capital);
        holder.textViewPopulation.setText(population);
        holder.textViewArea.setText(area);
        holder.textViewRegion.setText(region);
        holder.textViewSubregion.setText(subregion);
        String url = countryDetails.getFlag();

        Utils.fetchSvg(context, url, holder.flagView);

    }

    @Override
    public int getItemCount() {
        return infoList.size();
    }

    class CountryViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName, textViewCapital, textViewPopulation, textViewRegion, textViewSubregion, textViewArea;
        ImageView flagView;
        LinearLayout linearLayout;

        CountryViewHolder(View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.country_name);
            textViewCapital = itemView.findViewById(R.id.country_capital);
            textViewPopulation = itemView.findViewById(R.id.population);
            textViewRegion = itemView.findViewById(R.id.region);
            textViewSubregion = itemView.findViewById(R.id.subregion);
            flagView = itemView.findViewById(R.id.country_flag);
            linearLayout = itemView.findViewById(R.id.linear_layout);
            textViewArea = itemView.findViewById(R.id.area);

        }
    }
}
