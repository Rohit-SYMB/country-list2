package com.example.myglobe.CountryNetwork;

import com.example.myglobe.CountryModel.countryDetails;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ClientInstance {

    String BASE_URL="https://restcountries.eu";
    @GET("/rest/v2/name/{country}/")
    Call<List<countryDetails>>getAllDetails(@Path("country") String country);
}
